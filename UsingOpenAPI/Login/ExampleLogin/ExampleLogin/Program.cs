﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using RestSharp;

using System.Threading;

namespace ExampleLogin
{

    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;  

            string email, passwd, compr = "0S";

            Console.Write("email: ");
            email = Console.ReadLine();
            Console.Write("password: ");
            passwd = Console.ReadLine();

            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            Console.Write("Waiting connection");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.WriteLine(".");
            IRestResponse response = client.Execute(request);

            //verification in the response obtained that the data has been correct

            if (response.Content[10] == compr[0])
            {
                Console.WriteLine("Correct connection:");
                Console.WriteLine("Email: " + email);
                Console.WriteLine("Password: " + passwd);
                Console.WriteLine(response.Content);
            }
            else
            {
                Console.WriteLine("Error in the  Email: " + email + " or password");
                Console.WriteLine(response.Content);
            }

            Console.ReadKey();
        }
    }
}
