# Example StopBus Position in the map

_In this project I have created a example code that conects with MobilityLabs and checks the login.
Then the user needs to include the coordinates and search radius of points of interest around that position. Finally 
the program shows you all the points of interest that appear and the user must select which one you want to see on the map_

### Requirements 📋

_we need use  Microsoft Visual Studio for c#_

_Use the framework .NET 4.6.1_

_we need add package RestSharp.106.4.0_

_we need add package GMap.NET.WindowsForms.1.7.5_

_we need internet connection for connect with the API_

_We can also use PostMan, because Postman helps us to work with the APIis_

## Authors ✒️

* **Luis Muñoz** - *Innitial project, Documentation* - [Luismu02](https://gitlab.com/Luismu02)