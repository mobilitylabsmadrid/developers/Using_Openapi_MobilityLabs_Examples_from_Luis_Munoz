﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RestSharp;

namespace ArrivalTime
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validation();
           
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
                 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        public void Validation()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string email, passwd, compr = "0S", numToken, MensajeOrdenado = "";
            int  NumParadaMax, i = 0, j = 0, z = 41, zTime = z + 3, numeroDeBuses = 0, mostrarposBus = 0, mostrarposTime = 1, contadorDirParada = 0;
            long numVal, numLine;//If the user introduces a number very long
            email = this.Txt1.Text;
            passwd = this.Txt2.Text;
            //numVal = this.Number1.Text;

            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            IRestResponse response = client.Execute(request);
            //If the user introduces a word(char) this show a error and if introduces a number(int) is correct  Esto es por si el usuario mete los valores a mano en un textbox
             try
             {
                 numVal = Int64.Parse(this.Number1.Text);
                 numLine = Int64.Parse(this.Number2.Text);
            }
             catch (FormatException )
             {
                 numVal = -1;
                numLine = -1;
             }

            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);
            

            if (response.Content[10] == compr[0])
            {
                
                numToken = words[19];
                //List all the stops
                var client2 = new RestClient("https://openapi.emtmadrid.es/v1/transport/busemtmad/stops/all/list/");
                var request2 = new RestRequest(Method.GET);                
                request2.AddHeader("accessToken", numToken);
                IRestResponse response2 = client2.Execute(request2);

                //List all LineBuses
                var client4 = new RestClient("https://openapi.emtmadrid.es/v1/transport/busemtmad/lines/info/20180921/");
                var request4 = new RestRequest(Method.GET);               
                request4.AddHeader("accessToken", numToken);
                IRestResponse response4 = client4.Execute(request4);

                string text2 = response2.Content;
                string[] words2 = text2.Split(delimiterChars);
                string AcceptPetition ,ultimaParada, AcceptPetition2, ultimaLinea;

                string text4 = response4.Content;
                string[] words6 = text4.Split(delimiterChars);

                AcceptPetition = words2[3];
                AcceptPetition2 = words6[3];
                int lastLine = 0, ultimalineaMax = 0;
                if (AcceptPetition == "00" && AcceptPetition2 == "00") {
                    //primera parada NumParada = words2[9]; para la siguiente parada son 24 pos masNumParada = words2[33];
                   
                    ultimaParada = words2[i];
                    ultimaLinea = words6[lastLine];
                    while (ultimaParada != "datetime")
                    {
                        i++;
                        ultimaParada = words2[i];
                    }
                    i = i - 26 ;// this is the position where we have the last stop number


                    while (ultimaLinea != "datetime")
                    {
                        lastLine++;
                        ultimaLinea = words6[lastLine];
                    }
                    lastLine = lastLine - 6;// this is the position where we have the last BusLine
                    ultimaLinea = words6[lastLine];
                    ultimalineaMax = Int32.Parse(ultimaLinea);//get the last busline that exists Obtengo la ultima linea que existe 


                    //get the last stop that exists Obtengo la ultima parada que existe                    
                    ultimaParada = words2[i]; 
                    NumParadaMax = Int32.Parse(ultimaParada);

                    if (numVal > 0 && numVal <= NumParadaMax && numLine > 0 && numLine <= ultimalineaMax)
                    {

                        // pass to string because the line have 3 ddigit for example 001, 025, 128 Parseo el int a string porque las lineas son de tres digitos  
                        string ParseoLaLinea;
                        if(numLine < 10)
                        {
                            ParseoLaLinea = "00" + numLine.ToString();
                        }
                        else if( numLine >9 && numLine < 100)
                        {
                            ParseoLaLinea = "0" + numLine.ToString();
                        }
                        else
                        {
                            ParseoLaLinea = numLine.ToString();
                        }
        //              MessageBox.Show("parada existe y linea existe");

                        //HERE work on requesting stop information
                        string url = "https://openapi.emtmadrid.es/v1/transport/busemtmad/stops/" + numVal + "/arrives/" + ParseoLaLinea + "/";

                        var client3 = new RestClient(url);
                        var request3 = new RestRequest(Method.POST);

                        request3.AddHeader("accessToken", numToken);
                        request3.AddParameter("undefined", "{\n      \"statistics\":\"N\",\n      \"cultureInfo\":\"EN\",\n      \"Text_StopRequired_YN\":\"Y\",\n      \"Text_EstimationsRequired_YN\":\"Y\",\n      \"Text_IncidencesRequired_YN\":\"Y\",\n      \"DateTime_Referenced_Incidencies_YYYYMMDD\":\"20180823\"\n}", ParameterType.RequestBody);
                        IRestResponse response3 = client3.Execute(request3);
        //              MessageBox.Show(response3.Content);

                        string text3 = response3.Content;
                        string[] words3 = text3.Split(delimiterChars);
                        string MensajeFinal, mensajeInicial = "", mensajeInicialTime = " ", NoBuses = ": [], ", direccionParada = "", noLines = ": []}}, {";

                        // if don´t exist a stop between the stop 1 and the last stop 7396 Es para ver si no hay alguna parada entre medias de 0 y la final
                        NoBuses = words3[6];
                        noLines = words3[10];
                        if (NoBuses == ": [], ")
                        {
                            j = -1;
                        }
                        if (noLines == ": []}}, {")
                        {
                            j = -2;
                        }

                        if (j == -1)
                        {
                            richTextBox1.Text = "The stop doesn´t exist";
                        }
                        else if (j == -2)
                        {
                            richTextBox1.Text = "The BusLine doesn´t exist";
                        }
                        else {
                            // Here catch the position where we have last Bus

                            MensajeFinal = words3[j];
                            while (MensajeFinal != "StopLines")
                            {
                                j++;
                                MensajeFinal = words3[j];//153
                            }
                            j = j - 4;//this is the position where have last bus in the json. pos del ultimo bus en pasar por esa parada
                                        //}
                                        //First Bus  to came to the stop
                            while (z <= j)
                            {
                                mensajeInicial = mensajeInicial + words3[z] + '"';
                                mensajeInicialTime = mensajeInicialTime + words3[zTime] + '"';
                                z = z + 36;
                                zTime = z + 3;
                                numeroDeBuses++;
                            }

                            if (numeroDeBuses > 0) // if have Bus ->Show the busses
                            {

                                //              MessageBox.Show(mensajeInicial + mensajeInicialTime);                           
                                char[] delimiterChars2 = { '}', ',', '{', '"', ':' };
                                string[] words4 = mensajeInicial.Split(delimiterChars);
                                string[] words5 = mensajeInicialTime.Split(delimiterChars2);

                                //Create the message(mensajeOrdenado) that I will show
                                int timeinSec = Int32.Parse(words5[mostrarposTime]);
                                int timeInMin = timeinSec / 60, rest = timeinSec % 60;
                                if (rest >= 30)
                                {
                                    timeInMin++;
                                }

                                MensajeOrdenado = "Waiting time for " + numVal +" Stop and " + numLine + " BusLine:" + Environment.NewLine + Environment.NewLine;
                                MensajeOrdenado = MensajeOrdenado + words4[mostrarposBus] + ":  " + timeInMin + " Mins" + Environment.NewLine;
                                mostrarposBus++;
                                mostrarposTime = mostrarposBus + 5;

                                while (mostrarposBus < numeroDeBuses)
                                {
                                    timeinSec = Int32.Parse(words5[mostrarposTime]);
                                    timeInMin = timeinSec / 60;
                                    rest = timeinSec % 60;
                                    if (rest >= 30)
                                    {
                                        timeInMin++;
                                    }

                                    MensajeOrdenado = MensajeOrdenado + words4[mostrarposBus] + ":  " + timeInMin + " Mins" + Environment.NewLine;
                                    mostrarposBus++;
                                    mostrarposTime = mostrarposTime + 5;
                                }

                                direccionParada = words3[contadorDirParada];
                                while (direccionParada != "Direction")
                                {
                                    contadorDirParada++;
                                    direccionParada = words3[contadorDirParada];
                                }
                                contadorDirParada = contadorDirParada + 2;
                                direccionParada = words3[contadorDirParada];
                                richTextBox1.SelectionColor = Color.Red;
                                MensajeOrdenado = MensajeOrdenado + Environment.NewLine;
                                MensajeOrdenado = MensajeOrdenado + "Stop Direction: " + direccionParada + Environment.NewLine;
                                //Show the message
                                richTextBox1.Text = MensajeOrdenado;
                            }
                            else
                            {
                                //MessageBox.Show("Buses don´t pass");
                                richTextBox1.Text = "Buses don´t pass";
                            }
                        }
                    }
                    else
                    {
                        //MessageBox.Show("The stop doesn´t exist");
                        richTextBox1.Text = "The stop or line doesn´t exist";
                    }

      //            MessageBox.Show("correct");
                }
             }
             else
             {
                //MessageBox.Show("email  or password error");
                richTextBox1.Text = "email  or password error";
            }
            
        }

    }
    
}
