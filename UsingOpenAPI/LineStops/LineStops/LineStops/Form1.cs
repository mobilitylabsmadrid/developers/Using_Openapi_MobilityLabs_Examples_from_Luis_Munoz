﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

namespace LineStops
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Number1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validation();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        public void Validation()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string email, passwd, compr = "0S", numToken;
            string numLine; //If the user introduces a number very long
            email = this.textBox1.Text;
            passwd = this.textBox2.Text;
            numLine = this.Number1.Text;

            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            IRestResponse response = client.Execute(request);


            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);


            if (response.Content[10] == compr[0])
            {

                //List all LineBuses
                numToken = words[19];
                var client2 = new RestClient("https://openapi.emtmadrid.es/v1/transport/busemtmad/lines/info/20180921/");
                var request2 = new RestRequest(Method.GET);

                request2.AddHeader("accessToken", numToken);
                IRestResponse response2 = client2.Execute(request2);

                string text2 = response2.Content;
                string[] words2 = text2.Split(delimiterChars);
                string AcceptPetition;


                AcceptPetition = words2[3];
                int lastLine = 0;
                string ultimaLinea;
                bool encontrado = false;
                if (AcceptPetition == "00")
                {

                    ultimaLinea = words2[lastLine];

                    while (ultimaLinea != "datetime")
                    {
                        lastLine++;
                        ultimaLinea = words2[lastLine];
                    }
                    lastLine = lastLine - 11; //this is the position where we have the last BusLine
                    ultimaLinea = words2[lastLine];
                    int buscLinea = 25;

                    while (buscLinea < lastLine && !encontrado)
                    {
                        if (numLine == words2[buscLinea])
                        {
                            encontrado = true;
                        }
                        else
                        {
                            buscLinea = buscLinea + 28;
                        }
                        //              string most = words2[buscLinea];

                    }

                    if (encontrado)
                    {
                        //              MessageBox.Show("linea existe");
                        int dir = 1;
                        while (dir < 3)
                        {
                            string MensajeFinalMostar = "", MensajeFinalMostar2 = "";
                            //HERE work on requesting stop information
                            string url = "https://openapi.emtmadrid.es/v1/transport/busemtmad/lines/" + numLine + "/stops/" + dir + "/";
                            var client3 = new RestClient(url);
                            var request3 = new RestRequest(Method.GET);

                            request3.AddHeader("accessToken", numToken);
                            IRestResponse response3 = client3.Execute(request3);

                            //              MessageBox.Show(response3.Content);

                            string text3 = response3.Content;
                            string[] words3 = text3.Split(delimiterChars);

                            string PosComienzoStops = "", NombreParada = "";
                            // if don´t exist a stop between the stop 1 and the last stop 7396 Es para ver si no hay alguna parada entre medias de 0 y la final
                            int j = 0, i = 0;

                            PosComienzoStops = words3[j];
                            while (PosComienzoStops != "stops")
                            {
                                j++;
                                PosComienzoStops = words3[j];//153
                            }
                            j--; //pos que dice la primera parada
                            PosComienzoStops = words3[j];// estoy en la pos stops comienzo del array con todas las parada


                            i = j; //para localizar la ultima parada
                            NombreParada = words3[j];//comenzar busqueda de paradas

                            while (PosComienzoStops != "label")
                            {
                                i++;
                                PosComienzoStops = words3[i];//153
                            }
                            i--; //pos que dice la ultima parada
                            PosComienzoStops = words3[i];//palabra label

                            i = i - 4; // pos de la ultima parada

                            while (j < i)
                            {
                                PosComienzoStops = words3[j];
                                while (PosComienzoStops != "stop")
                                {
                                    j++;
                                    PosComienzoStops = words3[j];

                                }
                                j = j + 2;

                                MensajeFinalMostar = MensajeFinalMostar + words3[j] + " ";

                                PosComienzoStops = words3[j];
                                while (PosComienzoStops != "postalAddress")
                                {
                                    j++;
                                    PosComienzoStops = words3[j];
                                }
                                j = j + 2;
                                MensajeFinalMostar = MensajeFinalMostar + words3[j] + Environment.NewLine; // muestra numero de parada y direccion

                            }
                            if (dir == 1)
                            {
                                MensajeFinalMostar2 = "Direccion 1 Stops:" + Environment.NewLine + Environment.NewLine;
                                MensajeFinalMostar2 = MensajeFinalMostar2 + MensajeFinalMostar;
                                richTextBox1.Text = MensajeFinalMostar2;
                            }
                            else
                            {
                                MensajeFinalMostar2 = "Direccion 2 Stops:" + Environment.NewLine + Environment.NewLine;
                                MensajeFinalMostar2 = MensajeFinalMostar2 + MensajeFinalMostar;
                                richTextBox2.Text = MensajeFinalMostar2;
                            }
                            dir++;
                        }
                    }
                }
                else
                {
                    //MessageBox.Show("The stop doesn´t exist");
                    richTextBox1.Text = "The BusLine doesn´t exist";
                }

                //            MessageBox.Show("correct");
            }
            else
            {
                //MessageBox.Show("email  or password error");
                richTextBox1.Text = "email  or password error";
            }
        }

    }
}
