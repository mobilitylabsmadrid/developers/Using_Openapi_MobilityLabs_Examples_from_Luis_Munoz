﻿namespace LibDDP
{
    public enum DDPType
    {
        Ready,
        Added,
        Changed,
        Removed,
        Error,
        Connected,
        MethodResult,
        Updated
    }
}
