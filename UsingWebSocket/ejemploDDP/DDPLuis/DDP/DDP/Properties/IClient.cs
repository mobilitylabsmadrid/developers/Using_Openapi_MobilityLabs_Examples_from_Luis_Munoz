﻿using System;
namespace LibDDP
{
    public interface IClient
    {
        void AddItem(string jsonItem);
        bool Connect(string url, bool secure, Action closedhandler, Action<Exception> errorhandler);
        //int Call(string methodName, object arg);
        int Call(string methodName, object[] arg);
        int Subscribe(string methodName, string[] args);
        int Subscribe(string methodName, object[] args);
        int GetCurrentRequestId();
    }
}
