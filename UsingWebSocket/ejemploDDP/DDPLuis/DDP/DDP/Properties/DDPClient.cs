﻿using System;
using System.Text;

namespace LibDDP
{
    public class DDPClient : IClient
    {
        public const string DDP_MESSAGE_TYPE_READY = "ready";
        public const string DDP_MESSAGE_TYPE_ADDED = "added";
        public const string DDP_MESSAGE_TYPE_CHANGED = "changed";
        public const string DDP_MESSAGE_TYPE_NOSUB = "nosub";

        public const string DDP_PROPS_MESSAGE = "msg";
        public const string DDP_PROPS_ID = "id";
        public const string DDP_PROPS_COLLECTION = "collection";
        public const string DDP_PROPS_FIELDS = "fields";
        public const string DDP_PROPS_SESSION = "session";
        public const string DDP_PROPS_RESULT = "result";
        public const string DDP_PROPS_UPDATED = "updated";
        public const string DDP_PROPS_ERROR = "error";
        public const string DDP_PROPS_SUBS = "subs";

        private readonly DDPConnector _connector;
        private int _uniqueId;
        private readonly ResultQueue _queueHandler;

        public DDPClient(IDataSubscriber subscriber)
        {
            _connector = new DDPConnector(this);
            _queueHandler = new ResultQueue(subscriber);
            _uniqueId = 1;
        }

        public void AddItem(string jsonItem)
        {
            _queueHandler.AddItem(jsonItem);
        }

        public bool Connect(string url, bool secure, Action closedhandler, Action<Exception> errorhandler)
        {
            return _connector.Connect(url, secure, closedhandler, errorhandler);
        }

        public int Call(string methodName, params object[] args)
        {
            int req = NextId();
            string message = string.Format("\"msg\": \"method\",\"method\": \"{0}\",\"params\": [{1}],\"id\": \"{2}\"", methodName, CreateJSonArray(args), req);
            message = "{" + message + "}";
            _connector.Send(message.Replace("_mongo_","$"));
            return req;
        }

        public int Call(string methodName, params string[] args)
        {
            int req = NextId();
            string message = string.Format("\"msg\": \"method\",\"method\": \"{0}\",\"params\": [{1}],\"id\": \"{2}\"", methodName, CreateJSonArray(args), req);
            message = "{" + message + "}";
            message = message.Replace("\"{", "{");
            message = message.Replace("}\"", "}");
            message = message.Replace("'", "\"");
            _connector.Send(message);
            return req;
        }

        public int Subscribe(string subscribeTo, params string[] args)
        {
            string message = string.Format("\"msg\": \"sub\",\"name\": \"{0}\",\"params\": [{1}],\"id\": \"{2}\"", subscribeTo, CreateJSonArray(args), NextId());
            message = "{" + message + "}";
            _connector.Send(message);
            return GetCurrentRequestId();
        }

        public int Subscribe(string subscribeTo, params object[] args)
        {
            string message = string.Format("\"msg\": \"sub\",\"name\": \"{0}\",\"params\": [{1}],\"id\": \"{2}\"", subscribeTo, CreateJSonArray(args), NextId());
            message = "{" + message + "}";
            _connector.Send(message);
            return GetCurrentRequestId();
        }

        private string CreateJSonArray(params object[] args)
        {
            if (args == null)
                return string.Empty;
            StringBuilder argumentBuilder = new StringBuilder();
            string delimiter = string.Empty;
            for (int i = 0; i < args.Length; i++)
            {
                argumentBuilder.Append(delimiter);
                argumentBuilder.Append(Newtonsoft.Json.JsonConvert.SerializeObject(args[i]));
                delimiter = ",";
            }
            return argumentBuilder.ToString();
        }

        private string CreateJSonArray(params string[] args)
        {
            if (args == null)
                return string.Empty;

            StringBuilder argumentBuilder = new StringBuilder();
            string delimiter=string.Empty;
            for (int i = 0; i < args.Length; i++)
            {
                argumentBuilder.Append(delimiter);
                argumentBuilder.AppendFormat("\"{0}\"", args[i]);
                delimiter = ",";
            }
            
            return argumentBuilder.ToString();
        }
        private int NextId()
        {
            return _uniqueId++;
        }

        public int GetCurrentRequestId()
        {
            return _uniqueId;
        }

    }
}
