﻿using System;
using WebSocket4Net;
using System.Threading;

namespace LibDDP
{
    internal class DDPConnector
    {
        private WebSocket _socket;
        private string _url=string.Empty;
        private EventWaitHandle _waitHandle = new AutoResetEvent (false);  
        private bool _error = false;
        private readonly IClient _client;
        private Action ch;
        private Action<Exception> eh;

        public DDPConnector(IClient client)
        {
            this._client = client;
        }

        public bool Connect(string url, bool secure, Action closedhandler, Action<Exception> errorhandler)
        {
            _error = false;
            try
            {
                ch = closedhandler;
                eh = errorhandler;
                if (secure)
                    _url = "wss://" + url + "/websocket";
                else
                    _url = "ws://" + url + "/websocket";
                _socket = new WebSocket(_url);
                _socket.MessageReceived += socket_MessageReceived;
                _socket.Opened += _socket_Opened;
                _socket.Closed += _socket_Closed;
                _socket.Error += _socket_Error;
                _socket.Open();
                //_isWait = 1;
                //this.Wait();
                _waitHandle.WaitOne();
            }
            catch
            {
                _error = true;
            }
            return !_error;
        }

        public void Close()
        {
            _socket.Close();
        }

        public void Send(string message)
        {
            _socket.Send(message);
        }

        private const string _version = "pre1";

        void _socket_Opened(object sender, EventArgs e)
        {
            var message = string.Format("{{\"msg\":\"connect\",\"version\":\"{0}\",\"support\":[\"{0}\"]}}", _version);
            this.Send(message);
            //this.Send("{\"msg\":\"connect\",\"version\":\"pre1\",\"support\":[\"pre1\"]}");
            //_isWait = 0;
            _waitHandle.Set();
        }
        void _socket_Closed(object sender, EventArgs e)
        {
            if (ch != null) ch();
        }
        void _socket_Error(object sender, EventArgs e)
        {
            _error = true;
            _waitHandle.Set();
            if (eh != null)
            {
                eh((e as SuperSocket.ClientEngine.ErrorEventArgs).Exception);
            }
        }
        void socket_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            this._client.AddItem(e.Message);
        }

    }
}
