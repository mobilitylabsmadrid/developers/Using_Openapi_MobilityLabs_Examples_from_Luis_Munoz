﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

namespace Collection
{
    public partial class Form2 : Form
    {
        public string tokenrecibido, num2Seleccionado;
        public int numeroDeCategorias = 0;
        public int[] ContadorArray;
        public string[] Colecciones, CategoriaNombre, CategoriaNumero, SubCategoriaNombre, SubCategoriaNumero;
        public string NombreColeccion, nombreLink;
        int numeroS;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            InicializoCategoria(tokenrecibido);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            object num = comboBox1.SelectedItem;
            string catSeleccionada = comboBox1.SelectedItem.ToString();
            //MessageBox.Show(comboBox1.SelectedText);
            //MessageBox.Show(comboBox1.SelectedText);
            comboBox4.Items.Clear();
            comboBox4.ResetText();

            MuestroSubcategorias(tokenrecibido, catSeleccionada);
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            object num = comboBox2.SelectedItem;
            string subcatSeleccionada = comboBox2.SelectedItem.ToString();

            comboBox3.Items.Clear();
            comboBox3.ResetText();
            comboBox4.Items.Clear();
            comboBox4.ResetText();
            //MessageBox.Show(comboBox2.SelectedText);
            //cob2Seleccionado = comboBox2.SelectedText;
            MuestroColeccion(tokenrecibido, subcatSeleccionada);
            //MuestroColeccionCorrecta(tokenrecibido);
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

            object num = comboBox3.SelectedItem;
            string colSeleccionada = comboBox3.SelectedItem.ToString();
            string numeroSub = colSeleccionada;

            //MessageBox.Show(comboBox3.SelectedText);

            //string numeroSub = comboBox3.SelectedText;

            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collections/" + numeroS + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenrecibido);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            int lastCol = 0;
            string ultimaCol = "";
            while (ultimaCol != numeroSub)
            {
                lastCol++;
                ultimaCol = words2[lastCol];
            }
            while(words2[lastCol] != "CD_COLLECTION")
            {
                lastCol--;
            }
            NombreColeccion = words2[lastCol + 2 ];

            //LINKS
            comboBox4.Items.Clear();
            comboBox4.ResetText();
            MostrarLinks(tokenrecibido, NombreColeccion);



        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            object num = comboBox4.SelectedItem;
            string linkSeleccionada = comboBox4.SelectedItem.ToString();
            string numerolink = linkSeleccionada;

            //MessageBox.Show(comboBox3.SelectedText);

            //string numeroSub = comboBox3.SelectedText;

            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collection/" + NombreColeccion + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenrecibido);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            int lastCol = 0;
            string ultimaCol = "";
            while (ultimaCol != numerolink)
            {
                lastCol++;
                ultimaCol = words2[lastCol];
            }

            while(words2[lastCol] != "CD_LINK")
            {
                lastCol++;
            }
            nombreLink = words2[lastCol + 1];

            char[] delimiterChars2 = { ':', '}' };
            string[] nombreLink2 = nombreLink.Split(delimiterChars2);

            nombreLink = nombreLink2[1];


        }


        private void button1_Click(object sender, EventArgs e)
        {
            Validation(tokenrecibido);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        public void InicializoCategoria(string tokenr)
        {
            var client2 = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/discover/categories/");
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            int lastCat = 0;
            string ultimaCat;
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            string AcceptPetition;

            

            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                    ultimaCat = words2[lastCat];
                    while (ultimaCat != "description")
                    {
                        lastCat++;
                        ultimaCat = words2[lastCat];
                    }
                    //lastCat = lastCat - 34; //this is the position where we have the last categoria
                    while(words2[lastCat] != "DS_CATEGORY")
                    {
                        lastCat--;
                    }
                    ultimaCat = words2[lastCat];

                    int posCat = 0;

                    ultimaCat = words2[posCat];
                    while (posCat <= lastCat)
                    {
                        if (ultimaCat == "DS_CATEGORY")
                        {
                            numeroDeCategorias++;
                        }
                        posCat++;
                        ultimaCat = words2[posCat];
                    }
                    posCat = 0;
                    int meteDatos = 0, contArra = 0;
                    ContadorArray = new int[numeroDeCategorias];
                    CategoriaNombre = new string[numeroDeCategorias];
                    CategoriaNumero = new string[numeroDeCategorias];
                    SubCategoriaNombre = new string[numeroDeCategorias];
                    SubCategoriaNumero = new string[numeroDeCategorias];
                    for (int j = 0; j < numeroDeCategorias; j++)
                    {
                        ContadorArray[j] = contArra;
                    }
                    string GuardarNumero = "";
                    char[] delimiterChars2 = { ',', ':', '}' };
                    string[] words3, words4;
                    ultimaCat = words2[posCat];
                    while (posCat <= lastCat)
                    {
                        if (ultimaCat == "DS_CATEGORY")
                        {
                            
                            CategoriaNombre[meteDatos] = words2[posCat + 2];
                            SubCategoriaNombre[meteDatos] = words2[posCat + 8];

                            GuardarNumero = words2[posCat + 11];
                            words3 = GuardarNumero.Split(delimiterChars2);
                            CategoriaNumero[meteDatos] = words3[1];
                            
                            while(words2[posCat] != "CD_SUBCATEGORY")
                        {
                            posCat++;
                        }
                        posCat = posCat + 1;
                            GuardarNumero = words2[posCat];
                            words4 = GuardarNumero.Split(delimiterChars2);
                            SubCategoriaNumero[meteDatos] = words4[1];

                            for (int i = 0; i < numeroDeCategorias; i++)
                            {
                                if (CategoriaNombre[meteDatos] == CategoriaNombre[i])
                                {
                                    contArra = ContadorArray[i];
                                    contArra++;
                                    ContadorArray[i] = contArra;//este contador tiene las posiciones con categorias diferentes
                                }
                            }
                            meteDatos++;
                        }
                        posCat++;
                        ultimaCat = words2[posCat];
                    }

                    int z = 0;
                    int buscReact = 0, compReac = 0;
                    string valReactiva = "", comprobarReact = "";
                    while (ContadorArray[z] > 0)
                    {
                        valReactiva = words2[buscReact];
                        while(valReactiva != CategoriaNombre[z])
                        {
                            buscReact++;
                            valReactiva = words2[buscReact];
                        }
                        compReac = buscReact;
                        comprobarReact = words2[compReac];
                        while (comprobarReact != "CD_DATA_TYPE")
                        {
                            compReac++;
                            comprobarReact = words2[compReac];
                        }
                        compReac++;
                        comprobarReact = words2[compReac];
                    
                        if (comprobarReact == ": 3}, {" || comprobarReact == ": 3}], ")
                        {
                        int s = z ;
                            bool existe = false;
                            while(s > 0)
                            {
                              
                                if(CategoriaNombre[z] == CategoriaNombre[s - 1])
                                {
                                    existe = true;
                                }
                                s--;
                            }
                                if(!existe)
                                    comboBox1.Items.Add(CategoriaNombre[z]);
                        }
                        z++;
                    }
            }
        }

        public void MuestroSubcategorias(string tokenr, string catSel)
        {
            comboBox2.Items.Clear();
            comboBox3.Items.Clear();
            comboBox2.ResetText();
            comboBox3.ResetText();
            var client2 = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/discover/categories/");
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            int posCat = 0;
            string SubCat = "";
            char NumSub;
            string categoriaSeleccionada;
            



            int lastsCat = 0;
            string ultimasCat;
            ultimasCat = words2[lastsCat];
            while (ultimasCat != "description")
            {
                lastsCat++;
                ultimasCat = words2[lastsCat];
            }

            //lastsCat = lastsCat - 4; //this is the position where we have the last categoria
            while(words2[lastsCat] != "CD_SUBCATEGORY")
            {
                lastsCat--;
            }
            ultimasCat = words2[lastsCat];
            bool pasa = false;

            while(posCat <= lastsCat && !pasa) { 
                while (words2[posCat] != catSel && !pasa)
                {
                    posCat++;
                    if(posCat > lastsCat)
                        {
                            pasa = true;
                        }
                }
                posCat++;
                if (!pasa)
                {
                    while (words2[posCat] != "CD_SUBCATEGORY")
                    {
                        posCat++;

                    }
                    posCat++;
                    SubCat = words2[posCat];
                    NumSub = SubCat[2];//sacar el numero exacto de la subcategoria ya que tiene forma similar a (: 2 )--> para meter en la url


                    string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collections/" + NumSub + "/";
                    var client3 = new RestClient(url);
                    var request3 = new RestRequest(Method.GET);

                    request3.AddHeader("accessToken", tokenr);
                    IRestResponse response3 = client3.Execute(request2);
                    string text3 = response3.Content;
                    string[] words3 = text3.Split(delimiterChars);
                    int selSub = 0;
                    categoriaSeleccionada = catSel;


                    //comboBox1.SelectedText = comboBox1.SelectedText;
                    int buscoCategoriaSeleccionada = 0, numeroSubcategorias = 0;
                    while (CategoriaNombre[buscoCategoriaSeleccionada] != catSel)
                    {
                        buscoCategoriaSeleccionada++;
                    }
                    numeroSubcategorias = ContadorArray[buscoCategoriaSeleccionada];

                    for (int contadorSub = 0; contadorSub < numeroSubcategorias; contadorSub++)
                    {
                        for (int i = 0; i < numeroDeCategorias; i++)
                        {
                            if (categoriaSeleccionada == CategoriaNombre[i])
                            {
                                while (words3[selSub] != "CD_DATA_TYPE")
                                {
                                    selSub++;
                                }
                                selSub++;
                                if (words3[selSub] == ": 3, ")
                                {
                                    //comboBox2.Items.Add(SubCategoriaNombre[i]);
                                    comboBox2.Items.Add(words3[selSub - 31]);
                                    categoriaSeleccionada = "";
                                }
                            }
                        }
                    }





                }
            }

        }

        public void MuestroColeccion(string tokenr, string subcatSel)
        {
           
            //MessageBox.Show(comboBox2.SelectedText);
            int buscoCategoriaSeleccionada = 0;
            int numeroDeColecciones = 0;
            while (SubCategoriaNombre[buscoCategoriaSeleccionada] != subcatSel)
            {
                buscoCategoriaSeleccionada++;
            }
            num2Seleccionado = SubCategoriaNumero[buscoCategoriaSeleccionada];
            string numeroSub = SubCategoriaNumero[buscoCategoriaSeleccionada];
            numeroS = Int32.Parse(numeroSub);
        
            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collections/" + numeroS + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);

            string AcceptPetition;
            int lastCol = 0;
            string ultimaCol;
            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                ultimaCol = words2[lastCol];
                while (ultimaCol != "description")
                {
                    lastCol++;
                    ultimaCol = words2[lastCol];
                }
                // lastCol = lastCol - 14; //this is the position where we have the last coleccion
                while (words2[lastCol] != "DS_COLLECTION")
                {
                    lastCol--;
                }
                //ultimaCol = words2[lastCol];

                int posCol = 0;
                while (posCol <= lastCol)
                {
                    if (ultimaCol == "DS_COLLECTION")
                    {
                        numeroDeColecciones++;
                    }
                    posCol++;
                    ultimaCol = words2[posCol];
                }
                posCol = 0;
                Colecciones = new string[numeroDeColecciones];
                for(int i = 0; i < numeroDeColecciones; i++)
                {
                    //posCol = 0;
                    while (words2[posCol] != "DS_COLLECTION")
                    {
                        posCol++;
                    }
                    posCol = posCol + 2;
                    Colecciones[i] = words2[posCol];
                }
                for(int j = 0; j < numeroDeColecciones; j++)
                {
                    comboBox3.Items.Add(Colecciones[j]);
                }
            }

        }

        public void MostrarLinks(string tokenr, string nombreColeccion)
        {
            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collection/" + nombreColeccion + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenrecibido);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);



            string AcceptPetition;
            int lastlink = 0, numeroDeLinks = 0;
            string ultimolink;


            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                ultimolink = words2[lastlink];
                while (ultimolink != "comments")
                {
                    lastlink++;
                    ultimolink = words2[lastlink];
                }
                //lastlink = lastlink - 12; //this is the position where we have the last link
                while (words2[lastlink] != "CD_LINK_TYPE")
                {
                    lastlink--;
                }
                ultimolink = words2[lastlink];

                int poslink = 0;

                //ultimaCat = words2[posCat];
                while (poslink <= lastlink)
                {
                    ultimolink = words2[poslink];
                    if (ultimolink == "CD_LINK_TYPE")
                    {
                        numeroDeLinks++;//numero de links
                    }
                    poslink++;
                    ultimolink = words2[poslink];
                }

                int contBusc = 0, busquedaLink = 0;
                string valorLinkReac = "", NumeroLink = ""; ;
                while (contBusc < numeroDeLinks && numeroDeLinks != 0)
                {

                    valorLinkReac = words2[busquedaLink];
                    while (valorLinkReac != "CD_LINK_TYPE")
                    {
                        busquedaLink++;
                        valorLinkReac = words2[busquedaLink];
                    }
                    busquedaLink++;
                    valorLinkReac = words2[busquedaLink];
                    if (valorLinkReac == ": 6, ")//Si el cd_link_type es 6 que es el reactivo
                    {
                        valorLinkReac = words2[busquedaLink + 7];
                        NumeroLink = words2[busquedaLink + 10];
                        comboBox4.Items.Add(valorLinkReac);
                    }
                    contBusc++;//Paso  al siguiente link
                }


            }
        }

        public void Validation(string tokenr)
        {

            string NombreCol = NombreColeccion;
            string numeroLink = nombreLink;

            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/collection/reactive/" + NombreCol + "/" + numeroLink + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.POST);
           
            request2.AddHeader("accessToken", tokenr);

            string Consulta = this.parametrosDeConsulta.Text;

            request2.AddParameter("undefined", "{" + Consulta +" }", ParameterType.RequestBody);
            IRestResponse response2 = client2.Execute(request2);

            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);

            richTextBox1.Text = response2.Content;
        }
        }
}